import firebase from 'firebase';
require ("firebase/firestore");

// Your web app's Firebase configuration
var firebaseConfig = {
    apiKey: "AIzaSyB84ZTEQYSSweeh0qUlODV5cBl7iaS27xw",
    authDomain: "vue-shop-5e417.firebaseapp.com",
    databaseURL: "https://vue-shop-5e417.firebaseio.com",
    projectId: "vue-shop-5e417",
    storageBucket: "vue-shop-5e417.appspot.com",
    messagingSenderId: "17023700106",
    appId: "1:17023700106:web:49511684d0e7203e95b173",
    measurementId: "G-PVHEBNRXQM"
  };
  // Initialize Firebase
  export const fb = firebase.initializeApp(firebaseConfig);
  export const db = firebase.firestore();
  export const fa = firebase.analytics();
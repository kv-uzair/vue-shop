import Vue from 'vue'
import App from './App.vue'
import router from './router'
import jQuery from 'jquery'
import VueFirestore from 'vue-firestore'


Vue.use(VueFirestore, {
	key: 'id', // the name of the property. Default is '.key'.
	enumerable: true //  whether it is enumerable or not. Default is true.
})
Vue.use(VueFirestore)

window.$ = window.jQuery = jQuery

import 'bootstrap'
import './assets/app.scss'
import {
	fb
} from './firebase'

import Swal from 'sweetalert2'
window.Swal = Swal

import VueCarousel from 'vue-carousel';
Vue.use(VueCarousel);

import Vue2Filters from 'vue2-filters'
Vue.use(Vue2Filters)


import store from './store.js'


const Toast = Swal.mixin({
	toast: true,
	position: 'top-end',
	showConfirmButton: false,
	timer: 3000,
	timerProgressBar: true,
	onOpen: toast => {
		toast.addEventListener('mouseenter', Swal.stopTimer)
		toast.addEventListener('mouseleave', Swal.resumeTimer)
	}
})
window.Toast = Toast

Vue.config.productionTip = false
Vue.component('Navbar', require('./components/Navbar.vue').default)
Vue.component('add-to-cart', require('./components/AddToCart.vue').default);
Vue.component('products-list', require('./sections/ProductList.vue').default);
Vue.component('mini-cart', require('./components/MiniCart.vue').default);

let app = ''

fb.auth().onAuthStateChanged(function (user) {
	if (!app) {
		new Vue({
			router,
			store,
			render: h => h(App)
		}).$mount('#app')
	}
})